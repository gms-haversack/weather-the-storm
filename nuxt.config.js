import { version } from './package.json';

export default {
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  vuetify: {
    theme: {
      dark: false,
    },
  },
  env: {
    version,
  },
};
