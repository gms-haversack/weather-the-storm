const seedRandom = require('seedrandom');

function getRandomInt(seeder, min, max) {
  const minVal = Math.ceil(min);
  const maxVal = Math.floor(max);
  return Math.floor(seeder() * (maxVal - minVal + 1)) + min;
}

module.exports = (seed) => {
  const seeder = seedRandom(seed);
  return ({
    d12() {
      return getRandomInt(seeder, 1, 12);
    },
    d20() {
      return getRandomInt(seeder, 1, 20);
    },
  });
};
