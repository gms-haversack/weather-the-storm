# Weather The Storm
An RPG weather generation system inspired by [Weather System][reddit_post_link] by [/u/JustLikeFM][reddit_user_link].

This project can be found at [gmshaversack.com][deployed_link]

## Systems Supported
The aim is to add support for different RPG systems.

Currently supported:
* Dungeons & Dragons - 5th Edition

## Setup

To run the project, clone this repo and use the following instructions

```
# Install dependencies
npm install

# Run in development mode (localhost:3000)
npm run dev

# Build and run in production mode
npm run build && npm start
```

## Preparing for a pull request
Before you create a pull request, please make sure you have run the following commands and that they run without error:

```
# Lint the code
npm run lint

# (Optional) Automatically fix the code, then lint
npm run lintfix && npm run lint
```

[reddit_post_link]: https://old.reddit.com/r/DnDBehindTheScreen/comments/7pvytn/weather_trackinggeneratingeffects_system_20/
[reddit_user_link]: https://old.reddit.com/user/JustLikeFM
[deployed_link]: https://gmshaversack.com
