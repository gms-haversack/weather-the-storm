module.exports = {
  root: true,
  env: {
    node: true,
  },
  plugins: ['vue'],
  extends: [
    '@nuxtjs',
    'airbnb-base',
    'plugin:nuxt/recommended',
  ],
  rules: {
    // from https://github.com/nuxt/eslint-config/blob/master/index.js
    'import/no-unresolved': 'off',
    'vue/attributes-order': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'on' : 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'on' : 'off',
  },
};
